# Módulo de etiquetas

Paquete para añadir etiquetas a otros paquetes. Permite que unas etiquetas se
relacionen con otras para crear navegaciones y filtros.

## Instalación

Los pasos para instalarlo son:

1. `composer require bittacora/bpanel4-related-tags`
2. `php artisan migrate`
3. [Definir tipos de etiquetas](#tipos-de-etiquetas))
4. [Relacionar etiquetas y modelos](#relacionar-etiqueta-con-modelo)
5. [Añadir enlaces al menú](#enlaces-en-el-men)
6. Actualizar modelos y controladores para [guardar las etiquetas de los modelos](#guardar-etiquetas-de-un-modelo)
7. Añadir el [componente de blade](#componente-de-blade) a las vistas


## Tipos de etiquetas

El campo `type_name` de la tabla `related_tags` hace referencia al campo `name`
de la tabla `related_tag_types`. Esta última tabla no tiene ninguna pantalla de
edición, ya que se usa para, desde un paquete, registrar un "tipo de etiqueta",
que sirve para agrupar distintas etiquetas en un módulo (por ejemplo). Estos tipos
de etiquetas deben definirse desde el seeder del módulo que va a usar las etiquetas,
o manualmente en la base de datos.

La columna `relation` de `related_tag_types` debe indicar el nombre usado en la
relación dinámica que se define en el siguiente apartado.

## Relacionar etiqueta con modelo
Para obtener los Modelos relacionados con una etiqueta, hay que incluir lo siguiente
en el ServiceProvider del módulo que quiere usar el módulo de etiquetas ([ver documentación sobre relaciones dinámicas](https://laravel.com/docs/9.x/eloquent-relationships#dynamic-relationships)):

```php
Tag::resolveRelationUsing('nombre_que_tendra_la_relacion', function ($tagModel) {
    return $tagModel->belongsToMany(Modelo::class, 'related_taggables', 'tag_id', 'related_taggable_id');
});
```

Cambiando `nombre_que_tendra_la_relacion` y `Modelo`, se podrá relacionar la etiqueta
con el modelo que nos interese, por ejemplo:

```php
Tag::resolveRelationUsing('products', function ($tagModel) {
    return $tagModel->belongsToMany(Product::class, 'related_taggables', 'tag_id', 'related_taggable_id');
});

// Después, se podrían obtener los productos asociados a una etiqueta con:
$tag->products;
```

En el comando de instalación del módulo que use las etiquetas, habrá que registrar los tipos, si
no se crean desde la base de datos. Esto se puede hacer a través de la fachada del módulo:

```php
RelatedTags::createTagType('nombre_del_tipo_de_etiqueta', 'nombre_de_la_relación');
```

`nombre_de_la_relación` será el nombre de la relación que hemos definido con `resolveRelationUsing` en el paso anterior.

## Obtener etiquetas por tipo

```php
RelatedTagsFacade::getTagsByType('nombre-del-tipo')
```

## Enlaces en el menú
Cada módulo debe registrar sus rutas para editar etiquetas, y deben seguir el
siguiente patrón:

> http://**dominio**/bpanel/etiquetas/**modulo**/**tipo-etiqueta**

En **modulo** pondremos el slug de la ruta en la que se listan los modelos del módulo,
por ejemplo, si la ruta para listar productos es `/bpanel/productos`, módulo será `productos`.

En **tipo-etiqueta** habrá que poner el valor del campo `name` en la tabla `related_tag_types`
correspondiente al tipo de etiqueta que queremos listar.

Un ejemplo sería: http://localhost/bpanel/etiquetas/productos/condiciones-piel

La ruta se registraría así desde el módulo de productos:

```php 
Route::prefix('bpanel/etiquetas/productos')->name('dermaviduals-products.')->middleware(['web', 'auth', 'admin-menu'])
    ->group(function () {
        Route::get('/condiciones-piel')->name('skin-conditions');
});
```

Y el enlace para el menú de administración así, desde el comando de instalación del
módulo de productos

```php 
$adminMenu->createAction(
    'dermaviduals-products',
    'Condiciones de la piel',
    'skin-conditions',
    'fal fa-allergies'
);
```

Hay que recordar que después de actualizar el menú habrá que ejecutar  `php artisan optimize:clear`
y cerrar y abrir sesión otra vez en bPanel.

# Guardar etiquetas de un modelo

Para facilitar el uso de este paquete, el guardado de las etiquetas se puede hacer
usando un par de traits: `Bittacora\Bpanel4\RelatedTags\Traits\HasRelatedTags` y `Bittacora\Bpanel4\RelatedTags\Traits\SavesRelatedTags`.

Al crear o actualizar un modelo que tenga etiquetas, usaremos el trait `SavesRelatedTags`, que se añadirá
al controlador o clase que se encargue de hacer el guardado del modelo:

```php
use Bittacora\Bpanel4\RelatedTags\Traits\SavesRelatedTags;

...

// En el método store (por ejemplo)
$this->saveRelatedTags(
    'nombre del tipo de etiqueta',
    $model,
    $idsDeLasEtiquetas
);
```

Para que el trait anterior funcione, hay que hacer un par de cambios en el modelo
al que se le quieren añadir las etiquetas. Debe implementar la interfaz `Bittacora\Bpanel4\RelatedTags\Contracts\HasRelatedTags`
y usar el trait `Bittacora\Bpanel4\RelatedTags\Traits\HasRelatedTags`:

```php
class ModeloConEtiquetas extends Model implements \Bittacora\Bpanel4\RelatedTags\Contracts\HasRelatedTags
{
    use Bittacora\Bpanel4\RelatedTags\Traits\HasRelatedTags;
    ...
```

Con esto ya funcionará el trait que guarda las etiquetas.

## Componente de blade

El paquete incluye un componente de blade que se puede añadir a las vistas de
creación y edición de un modelo que tenga etiquetas:

```html
<x-related-tags::select
        :name="'nombre del campo'"
        :type="'nombre del tipo de etiqueta'"
        :label="Label del campo en la vista"
        :selectedTags="$model?->relatedTags('nombre del tipo de etiqueta')->get()"
/>
```

`name` y `type` no tienen por qué ser iguales. `name` será el nombre del campo en
el formulario, y será el que habrá que usar en los `Request`, etc, mientras que `type`
es el nombre del tipo de etiqueta que hayamos definido en nuestro módulo.
