<?php

declare(strict_types=1);

namespace Bittacora\Bpanel4\RelatedTags\Contracts;

use Bittacora\Bpanel4\RelatedTags\Models\Tag;
use Illuminate\Database\Eloquent\Relations\BelongsToMany;

interface HasRelatedTags
{
    /**
     * @param class-string $tagTypeName
     * @return BelongsToMany<Tag>
     */
    public function relatedTags(string $tagTypeName): BelongsToMany;
}
