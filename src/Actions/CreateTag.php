<?php

declare(strict_types=1);

namespace Bittacora\Bpanel4\RelatedTags\Actions;

use Bittacora\Bpanel4\RelatedTags\Models\Tag;
use Bittacora\Bpanel4\RelatedTags\Validation\TagValidator;
use Illuminate\Database\Connection;
use Throwable;

class CreateTag
{
    public function __construct(
        private readonly TagValidator $tagValidator,
        private readonly Connection $db,
    ) {
    }

    /**
     * @param array<string,string> $data
     * @throws Throwable
     */
    public function execute(array $data): void
    {
        $this->db->beginTransaction();

        try {
            $this->tagValidator->validateTagCreation($data);
            $tag = new Tag();
            $tag->setName($data['name']);
            $tag->setActive('1' === $data['active']);
            $tag->setTypeName($data['type_name']);
            $tag->save();
            $tag->relatedTags()->sync($data['related_tags'] ?? []);
            $this->db->commit();
        } catch (Throwable $throwable) {
            $this->db->rollBack();
            throw $throwable;
        }
    }
}
