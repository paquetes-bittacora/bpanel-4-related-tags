<?php

declare(strict_types=1);

namespace Bittacora\Bpanel4\RelatedTags\Actions;

use Bittacora\Bpanel4\RelatedTags\Models\Tag;
use Bittacora\Bpanel4\RelatedTags\Validation\TagValidator;
use Illuminate\Database\Connection;
use Throwable;

class UpdateTag
{
    public function __construct(
        private readonly TagValidator $tagValidator,
        private readonly Connection $db,
    ) {
    }

    /**
     * @param array<string,string> $data
     * @throws Throwable
     */
    public function execute(array $data, Tag $tag): void
    {
        $this->db->beginTransaction();

        try {
            $this->tagValidator->validateTagUpdate($data);
            $tag->setLocale($data['language']);
            $tag->setName($data['name']);
            $tag->setActive(isset($data['active']) && '1' === $data['active']);
            $tag->relatedTags()->sync($data['related_tags'] ?? []);
            $tag->save();
            $this->db->commit();
        } catch (Throwable $throwable) {
            $this->db->rollBack();
            throw $throwable;
        }
    }
}
