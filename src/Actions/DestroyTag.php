<?php

declare(strict_types=1);

namespace Bittacora\Bpanel4\RelatedTags\Actions;

use Bittacora\Bpanel4\RelatedTags\Models\Tag;

class DestroyTag
{
    public function execute(Tag $tag): void
    {
        $tag->relatedTags()->detach();
        $tag->parentTags()->detach();
        $tag->delete();
    }
}
