<?php

declare(strict_types=1);

namespace Bittacora\Bpanel4\RelatedTags;

use Illuminate\Support\Facades\Facade;

class RelatedTagsFacade extends Facade
{
    protected static function getFacadeAccessor(): string
    {
        return RelatedTags::class;
    }
}
