<?php

declare(strict_types=1);

namespace Bittacora\Bpanel4\RelatedTags\View\Components;

use Bittacora\Bpanel4\RelatedTags\Models\Tag;
use Illuminate\Contracts\View\Factory;
use Illuminate\Contracts\View\View;
use Illuminate\Support\Collection;
use Illuminate\View\Component;

class Select extends Component
{
    /**
     * @var Collection<int, Tag>
     */
    public Collection $availableTags;

    /**
     * @psalm-param Collection<int, Tag> $selectedTags
     */
    public function __construct(
        private readonly Factory $view,
        public readonly string $name,
        public readonly string $type,
        public readonly string $label,
        public readonly ?Collection $selectedTags,
    ) {
        $this->availableTags = Tag::where('type_name', '=', $this->type)
            ->where('active', '=', '1')->pluck('name', 'id');
    }

    public function render(): View
    {
        return $this->view->make('related-tags::bpanel.components.select');
    }
}
