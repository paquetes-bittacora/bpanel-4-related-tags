<?php

declare(strict_types=1);

namespace Bittacora\Bpanel4\RelatedTags;

use Bittacora\Bpanel4\RelatedTags\Commands\InstallCommand;
use Bittacora\Bpanel4\RelatedTags\Http\Livewire\TagsDatatable;
use Bittacora\Bpanel4\RelatedTags\View\Components\Select;
use Blade;
use Illuminate\Support\ServiceProvider;
use Livewire\Livewire;

class Bpanel4TagsServiceProvider extends ServiceProvider
{
    private const PACKAGE_PREFIX = 'related-tags';

    public function boot(): void
    {
        $this->commands([InstallCommand::class]);
        $this->loadMigrationsFrom(__DIR__ . '/../database/migrations');
        $this->loadRoutesFrom(__DIR__ . '/../routes/web.php');
        $this->loadViewsFrom(__DIR__ . '/../resources/views/', self::PACKAGE_PREFIX);
        $this->loadTranslationsFrom(__DIR__ . '/../resources/lang', self::PACKAGE_PREFIX);

        Livewire::component(self::PACKAGE_PREFIX . '::livewire.tags-table', TagsDatatable::class);
        Blade::component(self::PACKAGE_PREFIX . '::select', Select::class);
    }
}
