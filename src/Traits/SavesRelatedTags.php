<?php

namespace Bittacora\Bpanel4\RelatedTags\Traits;

use Bittacora\Bpanel4\RelatedTags\Contracts\HasRelatedTags;
use Bittacora\Bpanel4\RelatedTags\Models\Tag;
use RuntimeException;

trait SavesRelatedTags
{
    /**
     * @phpstan-param array<int|string> $tagIds
     */
    private function saveRelatedTags(string $typeName, HasRelatedTags $model, array $tagIds): void
    {
        if (!is_a($model, HasRelatedTags::class)) {
            throw new RuntimeException('El modelo debe usar el trait ' . HasRelatedTags::class);
        }

        $existingTags = $model->relatedTags($typeName)->get();
        if ($existingTags->isNotEmpty()) {
            $model->relatedTags($typeName)->detach($existingTags->pluck('id'));
        }

        $tags = Tag::findMany($tagIds);
        $model->relatedTags($typeName)->saveMany($tags);
    }
}
