<?php

declare(strict_types=1);

namespace Bittacora\Bpanel4\RelatedTags\Traits;

use Bittacora\Bpanel4\RelatedTags\Models\Tag;
use Illuminate\Database\Eloquent\Relations\BelongsToMany;

trait HasRelatedTags
{
    /**
     * @param class-string $tagTypeName
     * @return BelongsToMany<Tag>
     */
    public function relatedTags(string $tagTypeName): BelongsToMany
    {
        return $this->morphedByMany(
            Tag::class,
            'related_taggable',
            null,
            'related_taggable_id',
            'tag_id'
        )->where('type_name', '=', $tagTypeName);
    }
}
