<?php

declare(strict_types=1);

namespace Bittacora\Bpanel4\RelatedTags\Http\Livewire;

use Bittacora\Bpanel4\RelatedTags\Models\Tag;
use Illuminate\Database\Eloquent\Builder;
use Rappasoft\LaravelLivewireTables\DataTableComponent;
use Rappasoft\LaravelLivewireTables\Views\Column;

final class TagsDatatable extends DataTableComponent
{
    /** @var array<int>  */
    public array $selectedKeys;

    public string $moduleName;

    public string $tagType;

    /**
     * @return array<Column>
     */
    public function columns(): array
    {
        return [
            Column::make('Nombre', 'name')->addClass("w-30 text-center"),
            Column::make('Items relacionados', 'related_items')->addClass("w-30 text-center"),
            Column::make('Etiquetas relacionadas', 'related_tags')->addClass("w-30 text-center"),
            Column::make('Idiomas', 'language')->addClass("w-10 text-center"),
            Column::make('Activo', 'active')->addClass("w-10 text-center"),
            Column::make('Acciones')->addClass("w-10 text-center"),
        ];
    }

    /**
     * @return Builder<Tag>
     */
    public function query(): Builder
    {
        /** @var Builder<Tag> */
        return Tag::query()
            ->orderBy('name', 'DESC')
            ->where('type_name', '=', $this->tagType)
            ->when(
                $this->getFilter('search'),
                fn ($query, $term) => $query
                    ->where('name', 'like', '%' . $term . '%')
            );
    }

    public function rowView(): string
    {
        return 'related-tags::bpanel.livewire.related-tags-datatable';
    }

    /**
     * @return string[]
     */
    public function bulkActions(): array
    {
        return [
            'bulkDelete' => 'Eliminar',
        ];
    }

    public function bulkDelete(): void
    {
        if ([] !== $this->selectedKeys()) {
            Tag::destroy($this->selectedKeys);
            $this->resetAll();
        }
    }

    public function setTableRowClass(Tag $row): string
    {
        return 'rappasoft-centered-row';
    }
}
