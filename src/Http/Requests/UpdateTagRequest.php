<?php

declare(strict_types=1);

namespace Bittacora\Bpanel4\RelatedTags\Http\Requests;

use Bittacora\Bpanel4\RelatedTags\Validation\TagValidator;
use Illuminate\Foundation\Http\FormRequest;

class UpdateTagRequest extends FormRequest
{
    public function authorize(): bool
    {
        return true;
    }

    /**
     * @return array<string, string>
     */
    public function rules(TagValidator $tagValidator): array
    {
        return $tagValidator->getTagUpdateValidationFields();
    }
}
