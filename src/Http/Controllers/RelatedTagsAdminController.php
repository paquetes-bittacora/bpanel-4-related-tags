<?php

declare(strict_types=1);

namespace Bittacora\Bpanel4\RelatedTags\Http\Controllers;

use Bittacora\Bpanel4\RelatedTags\Actions\CreateTag;
use Bittacora\Bpanel4\RelatedTags\Actions\DestroyTag;
use Bittacora\Bpanel4\RelatedTags\Actions\UpdateTag;
use Bittacora\Bpanel4\RelatedTags\Http\Requests\CreateTagRequest;
use Bittacora\Bpanel4\RelatedTags\Http\Requests\UpdateTagRequest;
use Bittacora\Bpanel4\RelatedTags\Models\Tag;
use Illuminate\Contracts\Config\Repository;
use Illuminate\Contracts\View\View;
use Illuminate\Http\RedirectResponse;
use App\Http\Controllers\Controller;
use Illuminate\Routing\Redirector;
use Illuminate\Routing\Router;
use Illuminate\Routing\UrlGenerator;
use Illuminate\View\Factory;
use Route;
use Throwable;

class RelatedTagsAdminController extends Controller
{
    public function __construct(
        private readonly Factory $view,
        private readonly UrlGenerator $urlGenerator,
        private readonly Redirector $redirector,
        private readonly Router $route,
        private readonly Repository $config,
    ) {
    }

    public function index(string $moduleName, string $tagType): View
    {
        $this->authorize('related-tags.index');

        $packageRoute = $this->getPackageRoute($moduleName);

        return $this->view->make('related-tags::bpanel.index', [
            'moduleName' => $moduleName,
            'tagType' => $tagType,
            'tabs' => $this->getTabs($packageRoute, $moduleName, $tagType),
        ]);
    }

    public function create(string $moduleName, string $tagType): View
    {
        $this->authorize('related-tags.create');
        $packageRoute = $this->getPackageRoute($moduleName);

        return $this->view->make('related-tags::bpanel.create', [
            'action' => $this->urlGenerator->route('related-tags.store', [
                'moduleName' => $moduleName,
                'tagType' => $tagType,
            ]),
            'moduleName' => $moduleName,
            'tagType' => $tagType,
            'tag' => null,
            'tabs' => $this->getTabs($packageRoute, $moduleName, $tagType),
            'relatableTags' => $this->getRelatableTags(),
            'language' => $this->config->get('app.locale'),
        ]);
    }

    /**
     * @throws Throwable
     */
    public function store(CreateTagRequest $request, CreateTag $createTag): RedirectResponse
    {
        $this->authorize('related-tags.store');
        /** @var array{moduleName: string, tagType:string} $currentRouteParameters */
        $currentRouteParameters = $this->route->current()?->parameters();

        $createTag->execute($request->validated());

        return $this->redirector->route('related-tags.index', [
            'moduleName' => $currentRouteParameters['moduleName'],
            'tagType' => $currentRouteParameters['tagType'],
        ])->with(['alert-success' => __('related-tags::form.saved')]);
    }

    public function edit(string $moduleName, string $tagType, Tag $model, string $locale = 'es'): View
    {
        $this->authorize('related-tags.edit');
        $packageRoute = $this->getPackageRoute($moduleName);
        $model->setLocale($locale);

        return $this->view->make('related-tags::bpanel.edit', [
            'action' => $this->urlGenerator->route('related-tags.update', [
                'moduleName' => $moduleName,
                'tagType' => $tagType,
                'model' => $model,
            ]),
            'moduleName' => $moduleName,
            'tagType' => $tagType,
            'tag' => $model,
            'selectedTags' => $model->relatedTags()->pluck('id')->toArray(),
            'tabs' => $this->getTabs($packageRoute, $moduleName, $tagType),
            'relatableTags' => $this->getRelatableTags($model),
            'language' => $locale,
        ]);
    }

    public function update(
        UpdateTagRequest $request,
        UpdateTag $updateTag,
        string $moduleName,
        string $tagType,
        Tag $model
    ): RedirectResponse {
        $this->authorize('related-tags.update');

        $updateTag->execute($request->validated(), $model);

        return $this->redirector->route('related-tags.index', [
            'moduleName' => $moduleName,
            'tagType' => $tagType,
        ])->with(['alert-success' => __('related-tags::form.saved')]);
    }

    /**
     * @return array<array<string,string>>
     */
    private function getTabs(\Illuminate\Routing\Route $packageRoute, string $moduleName, string $tagType): array
    {
        return [
            [
                'permission' => $packageRoute->getName(),
                'title' => 'Volver a los contenidos',
                'icon' => 'fa fa-arrow-left',
                'route' => $packageRoute->getName(),
            ],
            [
                'permission' => 'related-tags.index',
                'title' => 'Listado de etiquetas',
                'icon' => 'fa fa-list',
                'route' => 'related-tags.index',
                'params' => [
                    'moduleName' => $moduleName,
                    'tagType' => $tagType,
                ],
            ],
            [
                'permission' => 'related-tags.create',
                'title' => 'Nueva etiqueta',
                'icon' => 'fas fa-plus',
                'route' => 'related-tags.create',
                'params' => [
                    'moduleName' => $moduleName,
                    'tagType' => $tagType,
                ],
            ],
        ];
    }

    public function destroy(
        DestroyTag $destroyTag,
        string $moduleName,
        string $tagType,
        Tag $model
    ): RedirectResponse {
        $this->authorize('related-tags.destroy');
        $destroyTag->execute($model);


        return $this->redirector->route('related-tags.index', [
            'moduleName' => $moduleName,
            'tagType' => $tagType,
        ])->with(['alert-success' => __('related-tags::form.deleted')]);
    }

    private function getPackageRoute(string $moduleName): \Illuminate\Routing\Route
    {
        $url = 'bpanel/' . $moduleName;
        return collect(Route::getRoutes())->first(function ($route) use ($url) {
            return $route->matches(request()->create($url));
        });
    }

    /**
     * @return array<string,string>
     */
    private function getRelatableTags(?Tag $currentTag = null): array
    {
        $query = Tag::where('active', '=', true);

        if (null !== $currentTag) {
            $query->where('id', '!=', $currentTag->getId());
        }

        return $query->get()->pluck('name', 'id')->toArray();
    }
}
