<?php

declare(strict_types=1);

namespace Bittacora\Bpanel4\RelatedTags\Commands;

use Illuminate\Console\Command;
use Spatie\Permission\Models\Permission;
use Spatie\Permission\Models\Role;

final class InstallCommand extends Command
{
    /**
     * @var string
     */
    protected $signature = 'bpanel4-related-tags:install';

    /**
     * @var string
     */
    protected $description = 'Instala el paquete para gestionar etiquetas relacionadas.';
    /**
     * @var string[]
     */
    private const PERMISSIONS = ['index', 'create', 'show', 'edit', 'destroy', 'store', 'update'];

    public function handle(): void
    {
        $this->comment('Instalando el módulo Related Tags...');

        $this->giveAdminPermissions();
    }

    private function giveAdminPermissions(): void
    {
        $this->comment('Añadiendo permisos...');
        /** @var Role $adminRole */
        $adminRole = Role::findOrCreate('admin');
        foreach (self::PERMISSIONS as $permission) {
            $permission = Permission::firstOrCreate(['name' => 'related-tags.'.$permission]);
            $adminRole->givePermissionTo($permission);
        }
    }
}
