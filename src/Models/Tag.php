<?php

declare(strict_types=1);

namespace Bittacora\Bpanel4\RelatedTags\Models;

use Eloquent;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\BelongsToMany;
use Illuminate\Support\Carbon;
use Spatie\Translatable\HasTranslations;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Bittacora\Bpanel4\RelatedTags\Models\Tag
 *
 * @property int $id
 * @property string $type_name
 * @property array $name
 * @property bool $active
 * @property Carbon|null $created_at
 * @property Carbon|null $updated_at
 * @property-read Collection|Tag[] $relatedTags
 * @property-read int|null $related_tags_count
 * @property-read RelatedTagType $type
 * @method static Builder|Tag newModelQuery()
 * @method static Builder|Tag newQuery()
 * @method static Builder|Tag query()
 * @method static Builder|Tag whereActive($value)
 * @method static Builder|Tag whereCreatedAt($value)
 * @method static Builder|Tag whereId($value)
 * @method static Builder|Tag whereName($value)
 * @method static Builder|Tag whereTypeName($value)
 * @method static Builder|Tag whereUpdatedAt($value)
 * @mixin Eloquent
 */
class Tag extends Model
{
    use HasTranslations;
    use SoftDeletes;

    /** @var string[] */
    public array $translatable = ['name'];

    public $table = 'related_tags';

    /**
     * @return BelongsToMany<Tag>
     */
    public function relatedTags(): BelongsToMany
    {
        return $this->belongsToMany(
            __CLASS__,
            $this->table . '_pivot',
            'parent_tag_id',
            'child_tag_id',
        );
    }

    /**
     * @return BelongsToMany<Tag>
     */
    public function parentTags(): BelongsToMany
    {
        return $this->belongsToMany(
            __CLASS__,
            $this->table . '_pivot',
            'child_tag_id',
            'parent_tag_id',
        );
    }

    /**
     * @return BelongsTo<RelatedTagType, Tag>
     */
    public function type(): BelongsTo
    {
        return $this->belongsTo(RelatedTagType::class, 'type_name', 'name');
    }

    /**
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }

    /**
     * @return string
     */
    public function getTypeName(): string
    {
        return $this->type_name;
    }

    /**
     * @param string $type_name
     */
    public function setTypeName(string $type_name): void
    {
        $this->type_name = $type_name;
    }

    /**
     * @return array
     */
    public function getName(): string
    {
        return $this->name;
    }

    public function setName(string $name): void
    {
        $this->name = $name;
    }

    public function isActive(): bool
    {
        return (bool)$this->active;
    }

    public function setActive(bool $active): void
    {
        $this->active = $active;
    }
}
