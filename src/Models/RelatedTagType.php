<?php

declare(strict_types=1);

namespace Bittacora\Bpanel4\RelatedTags\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Bittacora\Bpanel4\RelatedTags\Models\RelatedTagType
 *
 * @property int $id
 * @property string $name
 * @property string $relation_name
 * @method static \Illuminate\Database\Eloquent\Builder|RelatedTagType newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|RelatedTagType newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|RelatedTagType query()
 * @method static \Illuminate\Database\Eloquent\Builder|RelatedTagType whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|RelatedTagType whereName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|RelatedTagType whereRelationName($value)
 * @mixin \Eloquent
 */
class RelatedTagType extends Model
{
    use SoftDeletes;
}
