<?php

declare(strict_types=1);

namespace Bittacora\Bpanel4\RelatedTags\Validation;

use Illuminate\Validation\Factory;

class TagValidator
{
    public function __construct(private readonly Factory $validator)
    {
    }

    /** @return array<string, string> */
    public function getTagCreationValidationFields(): array
    {
        return [
            'name' => 'string|max:255|required',
            'active' => 'nullable|boolean',
            'related_tags' => 'nullable|array',
            'type_name' => 'required|string|exists:related_tag_types,name',
            'language' => 'required|string',
        ];
    }

    /** @return array<string, string> */
    public function getTagUpdateValidationFields(): array
    {
        return $this->getTagCreationValidationFields();
    }

    /**
     * @param array<string,string> $data
     */
    public function validateTagCreation(array $data): void
    {
        $this->validator->make($data, $this->getTagCreationValidationFields());
    }

    /**
     * @param array<string,string> $data
     */
    public function validateTagUpdate(array $data): void
    {
        $this->validator->make($data, $this->getTagUpdateValidationFields());
    }
}
