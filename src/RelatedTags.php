<?php

declare(strict_types=1);

namespace Bittacora\Bpanel4\RelatedTags;

use Bittacora\Bpanel4\RelatedTags\Models\Tag;
use Illuminate\Database\Connection;
use Illuminate\Support\Collection;

final class RelatedTags
{
    public function __construct(private readonly Connection $db)
    {
    }

    public function createTagType(string $name, string $relationName): void
    {
        $this->db->table('related_tag_types')->insert([
            'name' => $name,
            'relation_name' => $relationName,
        ]);
    }

    public function getTagsByType(string $typeName): Collection
    {
        return Tag::query()
            ->orderBy('name', 'DESC')
            ->where('type_name', '=', $typeName)->get();
    }
}
