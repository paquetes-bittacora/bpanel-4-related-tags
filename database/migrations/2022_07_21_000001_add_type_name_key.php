<?php

declare(strict_types=1);

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class () extends Migration {
    public function up(): void
    {
        Schema::table('related_tags', static function (Blueprint $table) {
            $table->string('type_name')->after('id');
            $table->foreign('type_name')->references('name')->on('related_tag_types');
        });
    }

    public function down(): void
    {
        Schema::table('related_tags', static function (Blueprint $table) {
            $table->dropForeign('type_name');
            $table->dropColumn('type_name');
        });
    }
};
