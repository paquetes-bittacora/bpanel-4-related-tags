<?php

declare(strict_types=1);

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class () extends Migration {
    public function up(): void
    {
        Schema::create('related_taggables', function (Blueprint $table) {
            $table->unsignedBigInteger('tag_id');
            $table->unsignedBigInteger('related_taggable_id');
            $table->string('related_taggable_type');
            $table->unique(['tag_id', 'related_taggable_id']);
        });
    }

    public function down(): void
    {
        Schema::dropIfExists('related_taggables');
    }
};
