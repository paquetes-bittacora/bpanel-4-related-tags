<?php

declare(strict_types=1);

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class () extends Migration {
    public function up(): void
    {
        Schema::create('related_tags_pivot', function (Blueprint $table) {
            $table->unsignedBigInteger('parent_tag_id');
            $table->unsignedBigInteger('child_tag_id');

            $table->foreign('parent_tag_id')->references('id')->on('related_tags');
            $table->foreign('child_tag_id')->references('id')->on('related_tags');
        });
    }

    public function down(): void
    {
        Schema::dropIfExists('related_tags_pivot');
    }
};
