<?php

declare(strict_types=1);

namespace Bittacora\Bpanel4\RelatedTags\Tests\Feature;

use Bittacora\Bpanel4\RelatedTags\Actions\CreateTag;
use Bittacora\Bpanel4\RelatedTags\Models\Tag;
use Bittacora\Bpanel4\RelatedTags\Tests\ObjectMothers\TagObjectMother;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Tests\TestCase;
use Throwable;

class CreateTagTest extends TestCase
{
    use RefreshDatabase;

    private CreateTag $createTag;
    private TagObjectMother $tagObjectMother;

    public function setUp(): void
    {
        parent::setUp();
        $this->createTag = $this->app->make(CreateTag::class);
        $this->tagObjectMother = new TagObjectMother($this->createTag);
    }

    /**
     * @throws Throwable
     */
    public function testSePuedeCrearUnaEtiqueta(): void
    {
        $savedTag = $this->tagObjectMother->createTagWithRandomName();
        $this->assertIsInt($savedTag->id);
    }

    /**
     * @throws Throwable
     */
    public function testSePuedeCrearUnaEtiquetaQueSeRelacionaConOtras(): void
    {
        $tagName = 'Etiqueta 1';
        $this->createTag->execute(['name' => $tagName, 'active' => '1', 'type_name' => 'type', 'related_tags' => []]);
        $relatedTag = Tag::where('name', 'LIKE', '%Etiqueta 1%')->firstOrFail();

        $newTag = $this->tagObjectMother->createTagWithRandomName(['related_tags' => [$relatedTag->id]]);

        $this->assertEquals('Etiqueta 1', $newTag->relatedTags()->firstOrFail()->getName());
    }
}
