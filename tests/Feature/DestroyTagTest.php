<?php

declare(strict_types=1);

namespace Bittacora\Bpanel4\RelatedTags\Tests\Feature;

use Bittacora\Bpanel4\RelatedTags\Actions\DestroyTag;
use Bittacora\Bpanel4\RelatedTags\Models\Tag;
use Bittacora\Bpanel4\RelatedTags\Tests\ObjectMothers\TagObjectMother;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Tests\TestCase;
use Throwable;

class DestroyTagTest extends TestCase
{
    use RefreshDatabase;

    private DestroyTag $destroyTag;
    private TagObjectMother $tagObjectMother;

    public function setUp(): void
    {
        parent::setUp();
        $this->destroyTag = $this->app->make(DestroyTag::class);
        $this->tagObjectMother = $this->app->make(TagObjectMother::class);
    }

    /**
     * @throws Throwable
     */
    public function testSePuedeEliminarUnaEtiqueta(): void
    {
        $tag = $this->tagObjectMother->createTagWithRandomName();
        $id = $tag->getId();

        $this->assertEquals($id, Tag::whereId($id)->first()?->getId());

        $this->destroyTag->execute($tag);

        $this->assertEmpty(Tag::whereId($id)->get()->all());
    }
}
