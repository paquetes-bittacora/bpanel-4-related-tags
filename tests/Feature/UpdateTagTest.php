<?php

declare(strict_types=1);

namespace Bittacora\Bpanel4\RelatedTags\Tests\Feature;

use Bittacora\Bpanel4\RelatedTags\Actions\UpdateTag;
use Bittacora\Bpanel4\RelatedTags\Models\Tag;
use Bittacora\Bpanel4\RelatedTags\Tests\ObjectMothers\TagObjectMother;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Tests\TestCase;

class UpdateTagTest extends TestCase
{
    use RefreshDatabase;

    private TagObjectMother $tagObjectMother;
    private UpdateTag $updateTag;

    public function setUp(): void
    {
        parent::setUp();
        $this->updateTag = $this->app->make(UpdateTag::class);
        $this->tagObjectMother = $this->app->make(TagObjectMother::class);
    }

    public function testSePuedeModificarUnaEtiqueta(): void
    {
        $tag = $this->tagObjectMother->createTagWithRandomName();
        $id = $tag->getId();
        $this->updateTag->execute([
            'language' => 'es',
            'name' => 'Nombre modificado',
            'related_tags' => [],
        ], $tag);

        $updatedTag = Tag::whereId($id)->firstOrFail();
        $this->assertEquals('Nombre modificado', $updatedTag->getName());
    }
}
