<?php

declare(strict_types=1);

namespace Bittacora\Bpanel4\RelatedTags\Tests\ObjectMothers;

use Bittacora\Bpanel4\RelatedTags\Actions\CreateTag;
use Bittacora\Bpanel4\RelatedTags\Models\Tag;
use Throwable;

class TagObjectMother
{
    public function __construct(private readonly CreateTag $createTag)
    {
    }

    /**
     * @throws Throwable
     */
    public function createTagWithRandomName(array $data = []): Tag
    {
        $tagName = 'Etiqueta ' . microtime();
        $data = array_merge([
            'name' => $tagName,
            'active' => '1',
            'type_name' => 'type',
            'related_tags' => [],
        ], $data);

        $this->createTag->execute($data);

        // Busco así porque usamos Translatable
        return Tag::where('name', 'LIKE', '%' . $tagName . '%')->firstOrFail();
    }
}
