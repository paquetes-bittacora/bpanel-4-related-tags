<?php

declare(strict_types=1);


use Bittacora\Bpanel4\RelatedTags\Http\Controllers\RelatedTagsAdminController;

Route::prefix('bpanel/etiquetas')->name('related-tags.')->middleware(['web', 'auth', 'admin-menu'])
    ->group(function () {
        Route::get('{moduleName}/{tagType}', [RelatedTagsAdminController::class, 'index'])->name('index');
        Route::get('{moduleName}/{tagType}/crear', [RelatedTagsAdminController::class, 'create'])->name('create');
        Route::post('{moduleName}/{tagType}/guardar', [RelatedTagsAdminController::class, 'store'])->name('store');
        Route::get('{moduleName}/{tagType}/{model}/editar/{locale?}', [RelatedTagsAdminController::class, 'edit'])->name('edit');
        Route::post('{moduleName}/{tagType}/{model}/actualizar', [RelatedTagsAdminController::class, 'update'])->name('update');
        Route::delete('{moduleName}/{tagType}/{model}/eliminar', [RelatedTagsAdminController::class, 'destroy'])->name('destroy');
    });
