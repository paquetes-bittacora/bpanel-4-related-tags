<div>
    @livewire('form::select', [
        'name' => $name . '[]',
        'allValues' => $availableTags,
        'labelText' => $label,
        'emptyValue' => true,
        'emptyValueText' => '-',
        'selectedValues' => $selectedTags?->pluck('id')->toArray(),
        'multiple' => true,
    ])
</div>
