@extends('bpanel4::layouts.bpanel-app')

@section('title', __('related-tags::form.new-product'))

@section('content')
    <x-dynamic-tabs :tabs="$tabs"/>
    <div class="card bcard">
        <div class="card-header bgc-primary-d1 text-white border-0">
            <h4 class="text-120 mb-0">
                <span class="text-90">{{ $panelTitle }}</span>
            </h4>
            @if(null === $tag?->id)
                @if(isset($language))
                    <img src="/img/flags/{{ $language }}.png">
                @endif
            @else
                @include('language::partials.form-languages', [
                    'model' => $tag,
                    'edit_route_name' => 'related-tags.edit',
                    'currentLanguage' => $language,
                    'additionalEditRouteParameters' => ['tagType' => $tag->type->name, 'moduleName' => $moduleName],
                ])
            @endif
        </div>
        <form class="mt-lg-3" autocomplete="off" method="post" action="{{ $action }}" enctype="multipart/form-data">
            @csrf
            @livewire('form::input-text', ['name' => 'name', 'labelText' => __('related-tags::tag.name'), 'required'=>true, 'value' => old('name') ?? $tag?->getName() ])
            @livewire('form::input-checkbox', ['name' => 'active', 'value' => 1, 'checked' => old('active') ?? $tag?->isActive(), 'labelText' => __('related-tags::tag.active'), 'bpanelForm' => true])
            @livewire('form::select', ['name' => 'related_tags[]', 'idField' => 'related_tags', 'allValues' => $relatableTags, 'selectedValues' => $selectedTags ?? [], 'labelText' => __('related-tags::tag.related-tags'), 'labelWidth' => '3', 'fieldWidth' => '7', 'multiple' => true, 'placeholder' => ''])
            <div class="row">
                <div class="col-3"></div>
                <div class="col-7">
                    <x-bpanel4-info message="Para asociar contenidos a esta etiqueta, debe ir a la pantalla de edición del contenido correspondiente."/>
                </div>
            </div>
            <div class="col-12 mt-5 border-t-1 bgc-secondary-l4 brc-secondary-l2 py-35 d-flex justify-content-center">
                @livewire('form::save-button',['theme'=>'update'])
                @livewire('form::save-button',['theme'=>'reset'])
            </div>
            <input type="hidden" name="type_name" value="{{ $tagType }}">
            <input type="hidden" name="language" value="{{ $language }}">
        </form>
    </div>

@endsection
