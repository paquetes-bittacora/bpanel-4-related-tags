@extends('bpanel4::layouts.bpanel-app')

@section('title', __('related-tags::form.new-tag'))

@section('content')
    <div class="card bcard">
        <div class="card-header bgc-primary-d1 text-white border-0">
            <h4 class="text-120 mb-0">
                <span class="text-90">{{ __('related-tags::form.new-tag') }}</span>
            </h4>
            @if(isset($language))
                <img src="/img/flags/{{ $language }}.png">
            @endif
        </div>
        <form class="mt-lg-3" autocomplete="off" method="post" action="{{ $action }}">
            @include('related-tags::bpanel._form', ['panelTitle' => __('related-tags::form.new-tag')])
        </form>
    </div>
@endsection
