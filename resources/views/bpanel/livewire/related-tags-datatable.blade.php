<td class="align-middle">
    <div class="text-center">
        {{$row->name}}
    </div>
</td>
<td class="align-middle">
    <div class="text-center">
        <span class="badge badge-primary">{{ $row->{$row->type->relation} !== null ? count($row->{$row->type->relation}) : 0 }}</span>
    </div>
</td>
<td class="align-middle">
    <div class="text-center">
        <span class="badge badge-primary">{{ count($row->relatedTags) }}</span>
    </div>
</td>
<td>
    @livewire('language::datatable-languages', ['model' => $row,
        'createRouteName' => 'related-tags.create',
        'additionalEditRouteParameters' => ['tagType' => $row->type->name, 'moduleName' => $moduleName],
        'additionalCreateRouteParameters' => ['tagType' => $row->type->name, 'moduleName' => $moduleName],
        'editRouteName' => 'related-tags.edit',], key('related-tag-language-'.$row->id))
</td>
<td class="align-middle">
    @livewire('utils::datatable-default', ['fieldName' => 'active', 'model' => $row, 'value' => $row->active, 'size' => 'xxs'], key('active-tag-'.$row->id))
</td>
<td class="align-middle">
    <div class="text-center">
        @livewire('utils::datatable-action-buttons', [
            'actions' => ["edit", "delete"],
            'scope' => 'related-tags',
            'model' => $row,
            'permission' => ['edit', 'delete'],
            'id' => $row->id,
            'message' => 'la etiqueta?',
            'routeParams' => [
                'edit' => [
                    'moduleName' => $moduleName,
                    'tagType' => $tagType,
                ],
                'destroy' => [
                    'moduleName' => $moduleName,
                    'tagType' => $tagType,
                ]
            ]
        ], key('tag-buttons-'.$row->id))
    </div>
</td>
