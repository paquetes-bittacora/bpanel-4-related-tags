@extends('bpanel4::layouts.bpanel-app')

@section('title', 'Etiquetas')

@section('content')
    <x-dynamic-tabs :tabs="$tabs"/>
    <div class="card bcard">
        <div class="card-header bgc-primary-d1 text-white border-0">
            <h4 class="text-120 mb-0">
                <span class="text-90">{{ __('related-tags::datatable.index') }}</span>
            </h4>
        </div>
        <div class="card-body">
            @livewire('related-tags::livewire.tags-table', ['moduleName' => $moduleName, 'tagType' => $tagType], key('related-tags-datatable'))
        </div>
    </div>
@endsection
