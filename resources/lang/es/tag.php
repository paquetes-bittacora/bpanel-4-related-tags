<?php

declare(strict_types=1);

return [
    'name' => 'Nombre',
    'active' => 'Activada',
    'related-tags' => 'Etiquetas relacionadas',
];
