<?php

declare(strict_types=1);

return [
    'new-tag' => 'Nueva etiqueta',
    'edit-tag' => 'Editar etiqueta',
    'error-saving' => 'Ocurrió un error al guardar la etiqueta',
    'saved' => 'La etiqueta se guardó correctamente',
    'deleted' => 'Etiqueta eliminada',
];
