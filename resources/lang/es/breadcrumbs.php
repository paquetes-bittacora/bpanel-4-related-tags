<?php

declare(strict_types=1);

return [
    'related-tags' => 'Etiquetas',
    'index' => 'Listado',
    'create' => 'Nueva',
    'edit' => 'Editar',
];
